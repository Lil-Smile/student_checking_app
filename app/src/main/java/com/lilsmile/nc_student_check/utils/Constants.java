package com.lilsmile.nc_student_check.utils;

/**
 * Created by Smile on 18.12.2016.
 */
public interface Constants {
    String ACTION = "action";
    String CREATE = "create";
    String EDIT = "edit";
    String ID = "id";
    int PLUS = 0;
    int MINUS = 1;
    String SIGN = "sign";

    int FINISH = 1488901;
}
