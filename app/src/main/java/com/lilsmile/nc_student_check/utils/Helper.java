package com.lilsmile.nc_student_check.utils;

import java.util.Calendar;

/**
 * Created by Smile on 18.12.2016.
 */
public class Helper {


    public static String convertTimestamp(long timestamp) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);

        StringBuilder builder = new StringBuilder();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        builder.append(day>10?day:"0"+day).append(".");
        builder.append(month>10?month:"0"+month).append(".");
        builder.append(year).append(" ");

        builder.append(hour>10?hour:"0"+hour).append(":");
        builder.append(minute>10?minute:"0"+minute);

        return  builder.toString();
    }
}
