package com.lilsmile.nc_student_check.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.lilsmile.nc_student_check.R;
import com.lilsmile.nc_student_check.classes.Student;
import com.lilsmile.nc_student_check.utils.Constants;

/**
 * Created by Smile on 18.12.2016.
 */
public class FinishDialog extends DialogFragment{

    long id;
    Activity activity;

    public FinishDialog() {}

    public void setData(Activity activity, long id) {
        this.id = id;
        this.activity = activity;

    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle(getString(R.string.finish));
        Student student = Student.findById(Student.class, id);
        builder.setMessage("Студент " + student.getName() + " " + student.getSurname() + " набрал необходимое число " + (student.getCountedEntity()==Constants.MINUS ? "минусов" : "плюсов") +
        "!\n" + "Настало время для " + student.getWinningAction() + "!");
        builder.setPositiveButton(getString(R.string.done), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });
        return builder.create();
    }
}
