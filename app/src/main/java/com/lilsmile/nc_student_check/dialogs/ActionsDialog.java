package com.lilsmile.nc_student_check.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.lilsmile.nc_student_check.R;
import com.lilsmile.nc_student_check.activities.AddOrEditStudent;
import com.lilsmile.nc_student_check.activities.StudentMore;
import com.lilsmile.nc_student_check.activities.StudentsList;
import com.lilsmile.nc_student_check.classes.Student;
import com.lilsmile.nc_student_check.utils.Constants;

/**
 * Created by Smile on 18.12.2016.
 */
public class ActionsDialog extends DialogFragment {

    long id;
    Activity activity;

    TextView buttonPlus, buttonDelete, buttonMinus, buttonEdit, buttonMore;

    public ActionsDialog(){}

    public void setData(long id, Activity activity) {
        this.id = id;
        this.activity = activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle(activity.getString(R.string.actions));

        View view = activity.getLayoutInflater().inflate(R.layout.actions_dialog, null);
        buttonPlus = (TextView) view.findViewById(R.id.buttonAddPlus);
        buttonMinus = (TextView) view.findViewById(R.id.buttonAddMinus);
        buttonDelete = (TextView) view.findViewById(R.id.buttonDelete);
        buttonEdit = (TextView) view.findViewById(R.id.buttonEdit);
        buttonMore = (TextView) view.findViewById(R.id.buttonMore);

        buttonPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddSign addSign = new AddSign();
                addSign.setData(Constants.PLUS, id, activity);
                addSign.show(getFragmentManager(), "plus");
                dismiss();
            }
        });

        buttonMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddSign addSign = new AddSign();
                addSign.setData(Constants.MINUS, id, activity);
                addSign.show(getFragmentManager(), "plus");
                dismiss();
            }
        });

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Student student = Student.findById(Student.class, id);
                student.delete();
                dismiss();
                if (activity instanceof StudentsList) {
                    ((StudentsList) activity).updateData();
                }
            }
        });

        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, AddOrEditStudent.class);
                intent.putExtra(Constants.ACTION, Constants.EDIT);
                intent.putExtra(Constants.ID, id);
                dismiss();
                activity.startActivity(intent);
            }
        });

        buttonMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, StudentMore.class);
                intent.putExtra(Constants.ID, id);
                activity.startActivity(intent);
                dismiss();

            }
        });

        builder.setView(view);
        return builder.create();

    }


}
