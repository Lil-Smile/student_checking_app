package com.lilsmile.nc_student_check.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.lilsmile.nc_student_check.R;
import com.lilsmile.nc_student_check.activities.StudentMore;
import com.lilsmile.nc_student_check.activities.StudentsList;
import com.lilsmile.nc_student_check.classes.Mark;
import com.lilsmile.nc_student_check.classes.Reason;
import com.lilsmile.nc_student_check.classes.Student;
import com.lilsmile.nc_student_check.utils.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Smile on 18.12.2016.
 */
public class AddSign extends DialogFragment implements Constants {

    int sign;
    Activity activity;
    long id;
    Student student;

    public AddSign(){

    }

    public void setData(int sign, long id, Activity activity) {
        this.sign = sign;
        this.id = id;
        this.activity = activity;
        student = Student.findById(Student.class, id);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        if (sign == PLUS) {
            builder.setTitle(getString(R.string.addPlus));
        } else {
            builder.setTitle(getString(R.string.addMinus));
        }

        View view = activity.getLayoutInflater().inflate(R.layout.add_sign_dialog, null);
        final EditText editText = (EditText) view.findViewById(R.id.etOtherSign);

        final RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.radioGroupReason);
        final List<Reason> reasonList = Reason.getReasonBySign(sign);
        final List<RadioButton> radioButtons = new ArrayList<>();
        int i = 0;
        for (Reason reason : reasonList) {
            RadioButton radioButton = new RadioButton(activity);
            radioButton.setText(reason.getReason());
            radioButton.setId(i);
            i++;
            radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    editText.setError(null);
                    if (isChecked) {
                        editText.setEnabled(false);
                    }
                }
            });
            radioButtons.add(radioButton);
            radioGroup.addView(radioButton);
        }

        RadioButton radioButton = new RadioButton(activity);
        radioButton.setText(activity.getString(R.string.other));
        radioButton.setId(i);
        radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    editText.setEnabled(true);
                } else {
                    editText.setEnabled(false);
                }
            }
        });
        radioButtons.add(radioButton);
        radioGroup.addView(radioButton);


        builder.setPositiveButton(getString(R.string.done), null);
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });

        builder.setView(view);

        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button b = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int idChecked = radioGroup.getCheckedRadioButtonId();

                        for (RadioButton radioButton : radioButtons) {
                            if (radioButton.getId()==idChecked) {
                                String reasonText = radioButton.getText().toString();
                                if (getString(R.string.other).equals(reasonText)) {
                                    reasonText = editText.getText().toString();
                                    if ((reasonText==null) || (reasonText.isEmpty())) {
                                        editText.setError(getString(R.string.neccessary));
                                        return;
                                    }
                                    Mark mark = new Mark();
                                    Reason reason = new Reason();
                                    reason.setSign(sign);
                                    reason.setReason(reasonText);
                                    reason.save();
                                    mark.setSign(sign);
                                    mark.setReason(reason);
                                    mark.setStudent(student);
                                    mark.setUsed(false);
                                    mark.setTimestamp(System.currentTimeMillis());
                                    mark.save();
                                    if (activity instanceof StudentsList) {
                                        ((StudentsList) activity).updateData();
                                    } else if (activity instanceof StudentMore) {
                                        ((StudentMore) activity).updateData();
                                    }
                                    dismiss();
                                    return;
                                }
                                Mark mark = new Mark();
                                Reason reason = Reason.find(Reason.class, "reason like ?", reasonText).get(0);
                                mark.setSign(sign);
                                mark.setReason(reason);
                                mark.setUsed(false);
                                mark.setStudent(student);
                                mark.setTimestamp(System.currentTimeMillis());
                                mark.save();
                                if (activity instanceof StudentsList) {
                                    ((StudentsList) activity).updateData();
                                } else if (activity instanceof StudentMore) {
                                    ((StudentMore) activity).updateData();
                                }
                                dismiss();
                            }
                        }
                    }
                });
            }
        });

        return dialog;
    }

}
