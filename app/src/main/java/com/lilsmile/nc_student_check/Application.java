package com.lilsmile.nc_student_check;

import com.orm.SugarContext;

/**
 * Created by Smile on 18.12.2016.
 */
public class Application extends android.app.Application {

    @Override
    public void onCreate() {
        SugarContext.init(this);
    }

    @Override
    public void onTerminate() {
        SugarContext.terminate();
    }
}
