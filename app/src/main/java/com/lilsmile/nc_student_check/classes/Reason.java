package com.lilsmile.nc_student_check.classes;

import com.lilsmile.nc_student_check.utils.Constants;
import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Smile on 18.12.2016.
 */
public class Reason extends SugarRecord implements Constants {

    private String reason;

    private int sign;

    public Reason(){}

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }



    public static List<Reason> getReasonBySign(int sign) {
        List<Reason> list =  Reason.find(Reason.class, "sign = ?", String.valueOf(sign));

        if (list.size()==0) {
            Reason reason = new Reason();
            reason.setReason("Сломал билд");
            reason.setSign(MINUS);
            reason.save();
            Reason reason1 = new Reason();
            reason1.setReason("Тикет закрыли");
            reason1.setSign(PLUS);
            reason1.save();
            Reason reason2 = new Reason();
            reason2.setReason("Реопнули тикет");
            reason2.setSign(MINUS);
            reason2.save();
            Reason reason3 = new Reason();
            reason3.setReason("Принес пиццу и поделился");
            reason3.setSign(PLUS);
            reason3.save();
            return Reason.getReasonBySign(sign);
        }
        return  list;
    }

    public int getSign() {
        return sign;
    }

    public void setSign(int sign) {
        this.sign = sign;
    }
}
