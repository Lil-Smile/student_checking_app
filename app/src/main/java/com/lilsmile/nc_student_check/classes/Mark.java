package com.lilsmile.nc_student_check.classes;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by Smile on 18.12.2016.
 */
@Table
public class Mark extends SugarRecord{

    public Mark(){}

    private Student student;
    private boolean used;
    private long timestamp;

    private Long id;

    private int sign; //0 - +, 1 - -

    private Reason reason;

    public int getSign() {
        return sign;
    }

    public void setSign(int sign) {
        this.sign = sign;
    }

    public Reason getReason() {
        return reason;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }
}
