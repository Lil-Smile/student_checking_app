package com.lilsmile.nc_student_check.classes;

import com.lilsmile.nc_student_check.utils.Constants;
import com.orm.SugarRecord;
import com.orm.dsl.Table;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Smile on 18.12.2016.
 */


@Table
public class Student  extends SugarRecord{

    private Long id;

    public Student(){}

    private String name;
    private String surname;
    private int neededNumber;
    private int countedEntity; // 0 - +, 1 - -
    private int scale; // 0 - const, other - progress
    private String winningAction;


    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getNeededNumber() {
        return neededNumber;
    }

    public void setNeededNumber(int neededNumber) {
        this.neededNumber = neededNumber;
    }

    public int getCountedEntity() {
        return countedEntity;
    }

    public void setCountedEntity(int countedEntity) {
        this.countedEntity = countedEntity;
    }

    public String getWinningAction() {
        return winningAction;
    }

    public void setWinningAction(String winningAction) {
        this.winningAction = winningAction;
    }

    public static List<Student> getStudents()
    {
        List<Student> list = new ArrayList<>();
        Iterator<Student> iterator = Student.findAll(Student.class);
        if (iterator.hasNext()) {
            do {
                list.add(iterator.next());
            } while (iterator.hasNext());
        }
        return list;
    }

    public int calcDelta() {

        List<Mark> marks = Mark.find(Mark.class, "student = ?", getId().toString());
        List<Mark> realMarks = new ArrayList<>();
        for (Mark mark : marks) {
            if (!mark.isUsed()) {
                realMarks.add(mark);
            }
        }
        int count = 0;
        for (Mark mark:realMarks) {
            if (mark.getSign()== Constants.PLUS) {
                count ++;
            } else {
                count --;
            }
        }
        int tmpNeeded = neededNumber;
        if (countedEntity==Constants.MINUS) {
            tmpNeeded = -tmpNeeded;
        }
        if (count==tmpNeeded) {
            setAllMarksAsUsed();
            return Constants.FINISH;
        }
        return count;
    }

    public void setAllMarksAsUsed() {
        List<Mark> marks = Mark.find(Mark.class, "student = ?", getId().toString());
        for (Mark mark : marks) {
            mark.setUsed(true);
            mark.save();
        }
        if (scale>0) {
            neededNumber+=scale;
        }
    }


    public int getScale() {
        return scale;
    }

    public void setScale(int scale) {
        this.scale = scale;
    }
}
