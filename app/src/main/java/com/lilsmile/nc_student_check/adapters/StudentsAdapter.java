package com.lilsmile.nc_student_check.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.lilsmile.nc_student_check.R;
import com.lilsmile.nc_student_check.activities.AddOrEditStudent;
import com.lilsmile.nc_student_check.classes.Student;
import com.lilsmile.nc_student_check.dialogs.FinishDialog;
import com.lilsmile.nc_student_check.utils.Constants;

import java.util.List;

/**
 * Created by Smile on 18.12.2016.
 */
public class StudentsAdapter extends BaseAdapter implements Constants{

    List<Student> data;
    Activity activity;

    public StudentsAdapter(List<Student> list, Activity activity)
    {
        this.data = list;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        if (position<=data.size()) {
            return data.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = activity.getLayoutInflater();
        Student student = data.get(position);


        convertView = layoutInflater.inflate(R.layout.one_student, null);

        TextView tvName, tvSurname, tvCurrentNumber;
        tvName = (TextView) convertView.findViewById(R.id.tvNameOneStudent);
        tvSurname = (TextView) convertView.findViewById(R.id.tvSurnameOneStudent);
        tvCurrentNumber = (TextView) convertView.findViewById(R.id.tvCurrentNumberOneStudent);



        tvName.setText(student.getName());
        tvSurname.setText(student.getSurname());
        int currentNumber = student.calcDelta();
        if (currentNumber == FINISH) {
            currentNumber = 0;
            FinishDialog dialog = new FinishDialog();
            dialog.setData(activity, student.getId());
            dialog.show(activity.getFragmentManager(), "finish");
        }
        tvCurrentNumber.setText(String.valueOf(currentNumber));
        if (currentNumber>0) {
            tvCurrentNumber.setTextColor(activity.getResources().getColor(R.color.greenColor));
        } else if (currentNumber<0) {
            tvCurrentNumber.setTextColor(activity.getResources().getColor(R.color.darkRedColor));
        } else {
            tvCurrentNumber.setTextColor(activity.getResources().getColor(R.color.blackColor));
        }

        return convertView;
    }
}
