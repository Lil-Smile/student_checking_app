package com.lilsmile.nc_student_check.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lilsmile.nc_student_check.R;
import com.lilsmile.nc_student_check.classes.Mark;
import com.lilsmile.nc_student_check.utils.Helper;

import java.util.List;

/**
 * Created by Smile on 18.12.2016.
 */
public class MarkAdapter extends BaseAdapter {
    List<Mark> marks;
    Activity activity;

    public MarkAdapter(List<Mark> marks, Activity activity) {
        this.marks = marks;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return marks.size();
    }

    @Override
    public Object getItem(int position) {
        return marks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = activity.getLayoutInflater();
        convertView = inflater.inflate(R.layout.one_mark, null);

        TextView tvTimestamp, tvContent;
        tvTimestamp = (TextView) convertView.findViewById(R.id.tvTimestampMark);
        tvContent = (TextView) convertView.findViewById(R.id.tvContentMark);

        Mark mark = marks.get(position);
        tvContent.setText(mark.getReason().getReason());
        tvTimestamp.setText(Helper.convertTimestamp(mark.getTimestamp()));


        return convertView;
    }
}
