package com.lilsmile.nc_student_check.activities;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RadioButton;

import com.lilsmile.nc_student_check.R;
import com.lilsmile.nc_student_check.classes.Student;
import com.lilsmile.nc_student_check.utils.Constants;

public class AddOrEditStudent extends ActionBarActivity implements Constants{

    AutoCompleteTextView tvName, tvSurname, tvCount, tvAction, tvKoef;
    RadioButton rbPlus, rbMinus, rbConst, rbProgress;
    Student student;
    ImageButton saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_edit_student);

        String action = getIntent().getStringExtra(ACTION);


        tvName = (AutoCompleteTextView) findViewById(R.id.tvStudentNameEditor);
        tvSurname = (AutoCompleteTextView) findViewById(R.id.tvSurnameEditor);
        tvCount = (AutoCompleteTextView) findViewById(R.id.tvNumberEditor);
        tvAction = (AutoCompleteTextView) findViewById(R.id.tvActionEditor);
        tvKoef = (AutoCompleteTextView) findViewById(R.id.tvKoef);

        rbPlus = (RadioButton) findViewById(R.id.rbPlus);
        rbMinus = (RadioButton) findViewById(R.id.rbMinus);
        rbConst = (RadioButton) findViewById(R.id.rbConst);
        rbProgress = (RadioButton) findViewById(R.id.rbProgress);

        saveButton = (ImageButton) findViewById(R.id.ibSaveEditor);

        if (CREATE.equals(action)) {
            student = new Student();
            setTitle(getString(R.string.create));
        } else if (EDIT.equals(action)){
            long id = getIntent().getLongExtra(ID, 0);
            setTitle(R.string.editing);
            student = Student.findById(Student.class, id);
            Log.wtf("id", id+"");
            tvName.setText(student.getName());
            tvSurname.setText(student.getSurname());
            if (student.getScale()==0) {
                rbConst.setChecked(true);
            } else {
                rbProgress.setChecked(true);
                tvKoef.setText(student.getScale()+"");
            }
            if (student.getCountedEntity()==0) {
                rbPlus.setChecked(true);
            } else {
                rbMinus.setChecked(true);
            }
            tvAction.setText(student.getWinningAction());
            tvCount.setText(student.getNeededNumber()+"");
        }



        rbConst.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    tvKoef.setVisibility(View.GONE);
                }
            }
        });
        rbProgress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    tvKoef.setVisibility(View.VISIBLE);
                }
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = tvName.getText().toString();
                if ((name==null) || (name.isEmpty())) {
                    tvName.setError(getString(R.string.neccessary));
                    return;
                }

                String surname = tvSurname.getText().toString();
                if ((surname==null) || (surname.isEmpty())) {
                    tvSurname.setError(getString(R.string.neccessary));
                    return;
                }

                int sign = rbPlus.isChecked()?0:1;
                String countString = tvCount.getText().toString();
                int count=3;
                if ((countString==null) || (countString.isEmpty())) {
                    tvCount.setError(getString(R.string.neccessary));
                    return;
                } else {
                    count=Integer.valueOf(countString);
                    if (count<3) {
                        count = 3;
                    }
                }

                int koef=0;
                if (rbProgress.isChecked()) {
                    String koefString = tvKoef.getText().toString();
                    if ((koefString==null) || (koefString.isEmpty())) {
                        tvKoef.setError(getString(R.string.neccessary));
                        return;
                    } else {
                        koef=Integer.valueOf(koefString);
                        if (koef<1) {
                            koef = 1;
                        }
                    }
                }

                String action = tvAction.getText().toString();
                if ((action==null) || (action.isEmpty())) {
                    action = getString(R.string.pizza);
                }

                student.setName(name);
                student.setSurname(surname);
                student.setCountedEntity(sign);
                student.setNeededNumber(count);
                student.setScale(koef);
                student.setWinningAction(action);
                student.save();
                Intent intent = new Intent(AddOrEditStudent.this, StudentsList.class);
                startActivity(intent);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_or_edit_student, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }
}
