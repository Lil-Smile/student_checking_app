package com.lilsmile.nc_student_check.activities;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import com.lilsmile.nc_student_check.R;
import com.lilsmile.nc_student_check.adapters.StudentsAdapter;
import com.lilsmile.nc_student_check.classes.Student;
import com.lilsmile.nc_student_check.dialogs.ActionsDialog;
import com.lilsmile.nc_student_check.utils.Constants;

import java.util.List;


public class StudentsList extends ActionBarActivity implements Constants{

    private ListView listView;
    List<Student> students;
    StudentsAdapter adapter;

    ImageButton ibAddStudent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students_list);

        setTitle(getString(R.string.listStudents));

        listView = (ListView) findViewById(R.id.lvStudents);
        ibAddStudent = (ImageButton) findViewById(R.id.buttonAddStudent);
        ibAddStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StudentsList.this, AddOrEditStudent.class);
                intent.putExtra(ACTION, CREATE);
                startActivity(intent);
            }
        });

        students = Student.getStudents();
        adapter = new StudentsAdapter(students, this);
        listView.setAdapter(adapter);
        listView.invalidate();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ActionsDialog dialog = new ActionsDialog();
                dialog.setData(students.get(position).getId(), StudentsList.this);
                dialog.show(getFragmentManager(), "actions");
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_students_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    public void updateData() {
        students = Student.getStudents();
        adapter = new StudentsAdapter(students, this);
        listView.setAdapter(adapter);
        listView.invalidate();
    }
}
