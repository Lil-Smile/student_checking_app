package com.lilsmile.nc_student_check.activities;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.lilsmile.nc_student_check.R;
import com.lilsmile.nc_student_check.adapters.MarkAdapter;
import com.lilsmile.nc_student_check.adapters.StudentsAdapter;
import com.lilsmile.nc_student_check.classes.Mark;
import com.lilsmile.nc_student_check.classes.Student;
import com.lilsmile.nc_student_check.dialogs.AddSign;
import com.lilsmile.nc_student_check.dialogs.FinishDialog;
import com.lilsmile.nc_student_check.utils.Constants;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StudentMore extends ActionBarActivity implements Constants{

    ListView listView;
    TextView tvCurrent;
    Student student;
    MarkAdapter adapter;
    List<Mark> markList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_more);

        listView = (ListView) findViewById(R.id.lvChronology);
        tvCurrent = (TextView) findViewById(R.id.tvCurrentNumberMore);

        long id = getIntent().getLongExtra(ID, 0);
        student = Student.findById(Student.class, id);

        setTitle(student.getName() + "  " + student.getSurname());

        int current = student.calcDelta();
        if (current == FINISH) {
            current = 0;
            FinishDialog dialog = new FinishDialog();
            dialog.setData(this, student.getId());
            dialog.show(this.getFragmentManager(), "finish");
        }
        tvCurrent.setText(current+"");
        if (current>0) {
            tvCurrent.setTextColor(getResources().getColor(R.color.greenColor));
        } else if (current<0) {
            tvCurrent.setTextColor(getResources().getColor(R.color.darkRedColor));
        } else {
            tvCurrent.setTextColor(getResources().getColor(R.color.blackColor));
        }

        markList = Mark.find(Mark.class, "student = ?", student.getId().toString());
        Collections.sort(markList, new Comparator<Mark>() {
            @Override
            public int compare(Mark o1, Mark o2) {
                long x = o1.getTimestamp();
                long y = o2.getTimestamp();
                return (x < y) ? 1 : ((x == y) ? 0 : -1);
            }
        });
        adapter = new MarkAdapter(markList, this);
        listView.setAdapter(adapter);
        listView.invalidate();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_student_more, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.add_menu_more) {
            AddSign addSign = new AddSign();
            addSign.setData(PLUS, student.getId(), this);
            addSign.show(getFragmentManager(), "plus");
        } else if (id == R.id.remove_menu_more) {
            AddSign addSign = new AddSign();
            addSign.setData(MINUS, student.getId(), this);
            addSign.show(getFragmentManager(), "plus");
        }

        return super.onOptionsItemSelected(item);
    }

    public void updateData() {
        int current = student.calcDelta();
        if (current == FINISH) {
            current = 0;
            FinishDialog dialog = new FinishDialog();
            dialog.setData(this, student.getId());
            dialog.show(this.getFragmentManager(), "finish");
        }
        tvCurrent.setText(current+"");
        if (current>0) {
            tvCurrent.setTextColor(getResources().getColor(R.color.greenColor));
        } else if (current<0) {
            tvCurrent.setTextColor(getResources().getColor(R.color.darkRedColor));
        } else {
            tvCurrent.setTextColor(getResources().getColor(R.color.blackColor));
        }
        tvCurrent.setText(current+"");
        markList = Mark.find(Mark.class, "student = ?", student.getId().toString());
        Collections.sort(markList, new Comparator<Mark>() {
            @Override
            public int compare(Mark o1, Mark o2) {
                long x = o1.getTimestamp();
                long y = o2.getTimestamp();
                return (x < y) ? 1 : ((x == y) ? 0 : -1);
            }
        });
        adapter = new MarkAdapter(markList, this);
        listView.setAdapter(adapter);
        listView.invalidate();
    }
}
